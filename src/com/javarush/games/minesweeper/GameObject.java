package com.games.minesweeper;
/*

 */

public class GameObject {
    public int x, y;
    public int countMineNeighbors;
    public boolean isFlag;
    public boolean isOpen;
    public boolean isMine;
     GameObject(int x, int y,boolean isMine) {
        this.x = x;
        this.y = y;
        this.isMine = isMine;
    }
}
