package com.games.minesweeper;


import javafx.animation.Animation;

import java.util.ArrayList;
import java.util.List;

public class MinesweeperGame extends Game {
    private static final int SIDE = 9;
    private GameObject[][] gameField = new GameObject[SIDE][SIDE];  // массив клеток
    private int countMinesOnField = 0;                              // счетчик мин
    private int countFlags = 0;                                     // счтечик флагов
    private int countClosedTiles = SIDE*SIDE;                       // счтечик первоначально закрытых полей
    private int score ;                                             // счтечик очков
    private static final String MINE = "\uD83D\uDCA3";              // константа с символом "мина"
    private static final String FLAG = "\uD83D\uDEA9";              // константа с символом "флаг"
    private boolean isGameStopped;                                  //константа на остановку игры

    @Override
    public void initialize() {
        setScreenSize(SIDE, SIDE);
        createGame();
    }
    //
    // создание игрового поля
    //
    private void createGame() {
        if (!isGameStopped) {
            for (int y = 0; y < SIDE; y++) {
                for (int x = 0; x < SIDE; x++)
                    setCellValue(x,y,"");
            }
        }
        for (int y = 0; y < SIDE; y++) {
            for (int x = 0; x < SIDE; x++) {
                boolean isMine = getRandomNumber(10) == 9;                     // рандом для флага isMine
                gameField[y][x] = new GameObject(x, y, isMine);                     // инициализация клеток объектами
                if (isMine) countMinesOnField++;                                    // инкремент счетчика мин
                setCellColor(x, y, Color.LIGHTBLUE);                                // установка изначального цвета клеток
            }
        }
        countFlags = countMinesOnField;         // инициальзация счетчика флагов количеством мин
        countMineNeighbors();                   // считаем количество содей-мин для всех клеток
    }

    //
    // клик левой кнопке мыши по клетке
    //
    @Override
    public void onMouseLeftClick(int x, int y){
        if(isGameStopped){
            restart();
        }
        else {
            openTile(x,y);
        }
    }
    //
    // клик правой кнопке мыши по клетке
    //
    @Override
    public void onMouseRightClick(int x, int y) {
        markTile(x,y);
    }


    //
    // лист с "соседями"
    //
    private List<GameObject> getNeighbors(GameObject gameObject) {
        List<GameObject> result = new ArrayList<>();
        for (int y = gameObject.y - 1; y <= gameObject.y + 1; y++) {
            for (int x = gameObject.x - 1; x <= gameObject.x + 1; x++) {
                if (y < 0 || y >= SIDE) {
                    continue;
                }
                if (x < 0 || x >= SIDE) {
                    continue;
                }
                if (gameField[y][x] == gameObject) {
                    continue;
                }
                result.add(gameField[y][x]);
            }
        }
        return result;
    }
    //
    // считает количество соседей-мин для каждой ячейки и записывает в поле countMineNeighbors соответствующего объекта GameObject
    //
    private void countMineNeighbors() {
        for (int y = 0; y < SIDE; y++) {
            for (int x = 0; x < SIDE; x++) {
                GameObject gameObject = gameField[y][x];
                if (!gameObject.isMine) {
                    for (GameObject neighbor : getNeighbors(gameObject)) {
                        if (neighbor.isMine)
                            gameObject.countMineNeighbors++;
                    }
                }
            }
        }
    }

    //
    //Открытие ячейки
    //
    private void openTile(int x, int y){

        GameObject gameObject = gameField[y][x];    //создаем игровой оюъект

        if(isGameStopped) return;                         //если ячейка уже флаг то выйти
        if(gameObject.isOpen||gameObject.isFlag) return;  //если ячейка уже открыта или флаг то выйти

        gameObject.isOpen = true;                   //ячейка открыта
        countClosedTiles--;                         //удаляем кол-во ячеек с поля потому что они открыты

        if(gameObject.isMine) {                     //если там мина
            setCellValueEx(x, y, Color.RED,MINE);   //рисуем мину
            setCellColor(x, y, Color.RED);          //раскрашиваем клетку в красный
            gameOver();
        }
        else {                                      //если ячейка не мина
            if(countClosedTiles==countMinesOnField)
                win();

            score+=5;                          //прибавляем 5 очков
            setScore(score);
            if(gameObject.countMineNeighbors==0){   //если сумма соседей == 0
                setCellValue(x, y, "");       //ничего не выводть в ячейку
                setCellColor(x, y, Color.GREENYELLOW);     //залить цвет зеленым
                List<GameObject> neighbors = getNeighbors(gameObject);  //neighbors - соседи из листа с GetNeighbors
                for (GameObject neighbor: neighbors) {                  //для всех элементов списка neighbors делаем следущ:
                    if(!neighbor.isOpen & !neighbor.isMine ){           //если соседние ячейки закрыты и в них нет мины
                        openTile(neighbor.x,neighbor.y);                //открываем список незанятых минами соседних ячеек
                    }
                }
            }
            else {                                                      //иначе, если сумма заминированых соседей !=0
                setCellNumber(x, y, gameObject.countMineNeighbors);     //внести в ячейку сумму заминированных соседей
                setCellColor(x, y, Color.GREENYELLOW);                   //заливаем ячейку
            }
        }
    }
    //
    //Помечаем ячейки флажком
    //
    private void markTile(int x, int y){
        if(isGameStopped){return;}

        GameObject gameObject = gameField[y][x];
        if(gameObject.isOpen)  //если ячейка уже открыта то продолжаем
            return;
        if(countFlags==0 & !gameObject.isFlag) // если сумма используемых флагов =0 и ячейка не флаг то..... хз
            return;

        if (!gameObject.isFlag){                    //если ячейка сейчас не флаг и на нее нажали
                gameObject.isFlag=true;             //флаг устанавливается
                countFlags--;                       //сумма использ.флажков уменьшается
                setCellValue(x, y, FLAG);           // ее рисунок меняется на флажок
                setCellColor(x,y,Color.VIOLET);     //цвет ее поля меняется на желтый
        } else {                                    //если ячейка уже флажок и на нее нажали
            gameObject.isFlag=false;                //флаг снимается
            countFlags++;                           //сумма использ.флажков увеличивается
            setCellValue(x,y,"");             //рисунок удаляется
            setCellColor(x,y,Color.LIGHTBLUE);       //возвращается исходный цвет
        }
    }

    //
    //Завершение игры
    //
    private void gameOver(){
        isGameStopped = true;
        showMessageDialog(Color.BLACK,"ВЫ ПРОИГРАЛИ", Color.WHITE,48); //Вывод сообщения о поражении
    }
    //
    //Перезапуск игры
    //
    private void restart() {
        isGameStopped = false;
        countClosedTiles = SIDE * SIDE;
        score = 0;
        countMinesOnField = 0;
        setScore(score);
        createGame();
    }
    //
    //Выйгрыш
    //
    private void win(){
        isGameStopped = true;
        showMessageDialog(Color.BLACK,"ВЫ ВЫЙГРАЛИ", Color.RED,48); //Вывод сообщения о победе

    }
}
