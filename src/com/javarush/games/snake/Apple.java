package com.games.snake;
import com.javarush.engine.cell.*;

public class Apple extends GameObject {
    public   Apple(int x, int y) {
        super(x, y);
    }
    private final static String APPLE_SIGN = "\uD83C\uDF4E";
    public void draw(Game game){
        game.setCellValueEx(x,y, Color.NONE, APPLE_SIGN, Color.GREEN, 75);

    }
    public boolean isAlive = true;

}
