package com.games.snake;
public enum Direction {
    UP, RIGHT, DOWN, LEFT;

    public Direction getOpposite() {
        switch (this) {
            case UP:
                return DOWN;
            case DOWN:
                return UP;
            case RIGHT:
                return LEFT;
            case LEFT:
                return RIGHT;
            default: //This will never happen
                return null;
        }
    }
}