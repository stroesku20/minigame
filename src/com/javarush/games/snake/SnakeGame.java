package com.games.snake;

import com.javarush.engine.cell.*;
import com.javarush.engine.cell.Game;

public class SnakeGame extends Game {

    public static final int WIDTH = 15;           //инициализируем ширину поля
    public static final int HEIGHT = 15;          //инициализируем высоту поля
    private Snake snake;                          //создаем поле snake
    private Apple apple;                          //создаем поле snake
    private int turnDelay;
    private boolean isGameStopped;
    private final static int GOAL = 28;
    private int score;

    @Override
    public void initialize() {
        setScreenSize(WIDTH, HEIGHT);                   //задаем размеры экрана
        createGame();
    }

    private void createGame() {
        snake = new Snake(WIDTH / 2, HEIGHT / 2); //создаем объект класса Snake и распологаем его по координатам x,y
        createNewApple();                               //создаем объект класса Apple и распологаем его по координатам x,y
        //apple.draw(this);
        isGameStopped = false;
        drawScene();
        turnDelay = 300;
        setTurnTimer(turnDelay);
        score = 0;
        setScore(score);

    }

//
// ОТРИСОВКА ЭКРАНА
//

    private void drawScene() {

            for (int x = 0; x < WIDTH; x++) {           //закрашиваем ячейки
                for (int y = 0; y < HEIGHT; y++)
                    setCellValueEx(x, y, Color.LIGHTBLUE, "");
            }
            snake.draw(this);                    //рисуем змейку
            apple.draw(this);                    //рисуем яблоко
        }

    @Override
    public void onTurn(int step) {
        snake.move(apple);
        if(!apple.isAlive) {                    //если яблоко не живое (съели)
            createNewApple();                   //создаем новое яблоко
            score = score + 5;                  //увеличиваем кол-во очков на 5
            setScore(score);                    //устанавливаем счетчик
            turnDelay = turnDelay - 10;         //уменьшаем время между шагом(перемещением змейки)
            setTurnTimer(turnDelay);            //задаем это время
        }

        if(!snake.isAlive) gameOver();
        if(snake.getLength() > GOAL)win();
        drawScene();
    }
//
//СЧИТЫВАЕМ НАЖАТИЯ КЛАВИШ
//
    @Override
    public void onKeyPress(Key key) {

        if (key == Key.ESCAPE){
            isGameStopped = true;
            showMessageDialog(Color.GREEN, "PAUSE",Color.BLACK,36);
            if(key == Key.ESCAPE){
                if (isGameStopped)
                isGameStopped = false;

            }
        }
        if (key == Key.SPACE && isGameStopped) {  //если нажата клавиша SPACE и игра остановлена то перезапустить ее
            createGame();
        }
        if (Key.LEFT == key) {
            snake.setDirection(Direction.LEFT);

        }
        if (Key.RIGHT == key) {
            snake.setDirection(Direction.RIGHT);
        }
        if (Key.UP == key) {
            snake.setDirection(Direction.UP);
        }
        if (Key.DOWN == key) {
            snake.setDirection(Direction.DOWN);
        }

    }
//
//СОЗДАНИЕ НОВЫХ ЯБЛОК
//
    private void createNewApple() {
        apple = new Apple(getRandomNumber(WIDTH), getRandomNumber(HEIGHT));     //создаем новое яблоко
        while(snake.checkCollision(apple)) {                                    //проверяем столкновение змейки и яблока, если true (нет столкновения)
            apple = new Apple(getRandomNumber(WIDTH), getRandomNumber(HEIGHT)); //создаем еще яблоко
    }
}

   private void gameOver() {
       stopTurnTimer();
       isGameStopped = true;
       showMessageDialog(Color.NONE, "GAME OVER", Color.RED, 52);
   }

    private void win(){
        stopTurnTimer();
        isGameStopped = true;
        showMessageDialog(Color.NONE,"YOU WIN",Color.LAWNGREEN,52);

    }
}
