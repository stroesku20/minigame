package com.games.snake;

import com.javarush.engine.cell.*;
import com.javarush.engine.cell.Game;

import java.util.ArrayList;
import java.util.List;


public class Snake {

    private List<GameObject> snakeParts = new ArrayList<>();    //сегменты змеи
    private static final String HEAD_SIGN = "\uD83D\uDC7E";     //отрисовка головы змеи
    private static final String BODY_SIGN = "\u26AB";          //отрисовка тела змеи
    public boolean isAlive = true;                             //змея жива
    private Direction direction = Direction.LEFT;              //направление

    //
    //конструктор Snake
    //
    public Snake(int x, int y) {
        GameObject snake = new GameObject(x, y);
        GameObject snake1 = new GameObject(x + 1, y);
        GameObject snake2 = new GameObject(x + 2, y);
        snakeParts.add(snake);
        snakeParts.add(snake1);
        snakeParts.add(snake2);
    }

    //
    //НАПРАВЛЕНИЕ
    //
    public void setDirection(Direction direction) {
          /*
        Допустим наша змейка в стандартном положении (двигается справа налево).
Если в промежуток между движениями мы сделаем следующее:
1) Нажимаем стрелку вниз (наш класс Snake принял наш "приказ" и уже записал в себя команду, требующую повернуть "вниз".НО.
На данный момент наша змейка ещё не отрисована, т.к. не прошли 300 мсек.
2) Нажимаем стрелку "вправо". Т.к. внутри наша змейка помнит предыдущую команду "вниз", то она спокойно даёт нам возможность повернуть направо.
Выходят наши 300 мсек и поступает команда "двигаться". И тут мы сталкиваемся с проблемой, ибо змейка "запомнила" нашу последнюю команду
 "вправо" и выполняет именно её. После чего с шумом и грохотом врезается сама в себя.
         */
          if (this.direction == Direction.LEFT && direction == Direction.RIGHT) return;
          if (this.direction == Direction.RIGHT && direction == Direction.LEFT) return;
          if (this.direction == Direction.UP && direction == Direction.DOWN) return;
          if (this.direction == Direction.DOWN && direction == Direction.UP) return;
        if (!(((this.direction == Direction.LEFT || this.direction == Direction.RIGHT) &&
                (snakeParts.get(0).x == snakeParts.get(1).x)) ||
                ((this.direction == Direction.UP ||
                  this.direction == Direction.DOWN) &&
                  (snakeParts.get(0).y == snakeParts.get(1).y))))
                  this.direction = direction;
    }

    //
    //ОТРИСОВКА ЗМЕИ
    //
    public void draw(Game game) {
        // if (!isAlive)   //если змея мертва - закрасить её голову
        Color color = isAlive ? Color.BLACK : Color.RED;            //если isAlive то Color.BLACK, else Color.RED
        game.setCellValueEx(snakeParts.get(0).x, snakeParts.get(0).y, Color.NONE, HEAD_SIGN, color, 75);     // рисуем голову

        for (int i = 1; i < snakeParts.size(); i++) {   //закашиваем каждый кусочек змеи
            game.setCellValueEx(snakeParts.get(i).x, snakeParts.get(i).y, Color.NONE, BODY_SIGN, color, 75); //рисуем все остальное тело
        }
    }

    //
    //ДВИЖЕМСЯ
    //
    public void move(Apple apple) {
        GameObject gameObject = createNewHead();
        //перед добавлением нового элемента(передвижения головы) - проверяем нет ли у него столкновений
        if (checkCollision(gameObject)) {                 //если столкновение есть
            isAlive = false;                              //то змея умерает
            return;
        }
        if (gameObject.x > SnakeGame.WIDTH - 1 ||         //проверяем, если голова змеи выходит за пределы поля
                gameObject.x < 0 ||
                gameObject.y > SnakeGame.HEIGHT - 1 ||
                gameObject.y < 0) {
            isAlive = false;                                //змея умерает
        } else {
            if (gameObject.x == apple.x && gameObject.y == apple.y) { //если координаты головы змеи попадают на координаты яблока
                apple.isAlive = false;                                //яблоко умерает(его съели)

                snakeParts.add(0, gameObject);                  //добавляем сегмент к змейке НЕ УДАЛЯЯ СЕГМЕНТ ХВОСТА, тем самым змейка растет
            } else {
                snakeParts.add(0, gameObject);              //иначе добавляем голову в остальным сегментам, в самое начало
                removeTail();                                     //удаляем сегмент хвоста
            }
        }
    }

    //
//УДАЛЯЕМ ХВОСТ
//
    public void removeTail() {
        snakeParts.remove(snakeParts.size() - 1); //удаляем последний сегмент хвоста змейки
    }

    //
//CОЗДАЁМ НОВУЮ ГОЛОВУ
//
    public GameObject createNewHead() {    //создаем новую голову в нужном нам направлении
        GameObject gameObject = null;

        if (direction == Direction.UP)
            gameObject = new GameObject(snakeParts.get(0).x, snakeParts.get(0).y - 1);   //если змейка движ вврх
        if (direction == Direction.DOWN)
            gameObject = new GameObject(snakeParts.get(0).x, snakeParts.get(0).y + 1);  //если змейка движ вниз
        if (direction == Direction.RIGHT)
            gameObject = new GameObject(snakeParts.get(0).x + 1, snakeParts.get(0).y);    //если змейка движ вправо
        if (direction == Direction.LEFT)
            gameObject = new GameObject(snakeParts.get(0).x - 1, snakeParts.get(0).y);   //если змейка движ влево
        return gameObject;
    }

    //
    //ПРОВЕРЯЕМ НА СТОЛКНОВЕНИЯ
    //
    public boolean checkCollision(GameObject gameObject) {
        for (GameObject obj : snakeParts) {         //проверяем каждый объект на столкновение с сегемнтом Snake
            if (gameObject.x == obj.x && gameObject.y == obj.y) {
                return true;                        //если столкновение (совпадение полей) обнаружено то столкновение true
            }
        }
        return false;                               //если столкновение (совпадение полей) не обнаружено то столкновение false
    }

    public int getLength(){
        return snakeParts.size();
    }
}

